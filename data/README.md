# Archivos de datos

Los archivos siguen la convención de nombre:
- `n_info.json` Información del n-ésimo destino
- `n_places.json` Listado de lugares del n-ésimo destino
- `n_m_info.json` Información del m-ésimo lugar del n-ésimo destino
